import PageObjectModule.CommonFns;
import cucumber.api.java8.En;

public class MyStepdefs implements En {
    public MyStepdefs() {
            CommonFns.beforeTest();
        Given("^Enter the phpTravels URL$", () -> {
            CommonFns.verifySearchHotel();
        });
        When("^Search the hotel list$", () -> {
            CommonFns.searchList();
        });
        Then("^Navigate back to Home page$", () -> {
            CommonFns.navigationWondows();
        });



        Given("^Search particular country$", () -> {
        CommonFns.bookFeatureTour();
        });
        When("^Book the package$", () -> {
        CommonFns.featuredBookingConfirmation();
        });
        Then("^Navigate till confirm Page$", () -> {
         CommonFns.bookingConfirmationSummary();
         CommonFns.quiteDriver();
        });

    }
}
