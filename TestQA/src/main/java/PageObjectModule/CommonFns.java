package PageObjectModule;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static io.github.bonigarcia.wdm.DriverManagerType.CHROME;

public class CommonFns {
    private static WebDriver driver;
    @FindBy(how = How.XPATH, using = "(//*[contains(text(),\"Search by Hotel or City Name\")])[1]")
    static WebElement searchHotel;
    @FindBy(how = How.XPATH, using = "//*[@name=\"checkin\"]")
    static WebElement clickCheckin;
    @FindBy(how = How.XPATH, using = "//*[@id=\"cookyGotItBtn\"]")
    static WebElement acceptCookies;
    @FindBy(how = How.XPATH, using = "(//*[@class=\" table-condensed\"])[4]//following::tr/td")
    static WebElement getDate;
    @FindBy(how=How.XPATH,using="(//*[@placeholder=\"Check in\"])[2]")
    static WebElement checkIn;
    @FindBy(how=How.XPATH,using="//*[@placeholder=\"Check out\"]")
    static WebElement checkOut;
    @FindBy(how=How.XPATH,using="//*[@name=\"travellers\"]")
    static WebElement members;
    @FindBy(how=How.XPATH,using="(//*[@type=\"submit\"])[4]")
    static WebElement searchSubmit;
    @FindBy(how=How.XPATH,using="//a[@class=\"navbar-brand go-right\"]")
    static WebElement navigateBackToHomePage;
    @FindBy(how=How.XPATH,using="(//*[@class=\"container\"])[5]//following::h4/span")
    static List<WebElement> featuredTourCount;
    @FindBy(how = How.XPATH,using = "(//*[@name=\"date\"])[1]")
    static WebElement changeFetaureBookingDate;
    @FindBy(how=How.XPATH,using = "//*[@id=\"selectedAdults\"]")
    static WebElement adultDropDownList;
    @FindBy(how=How.XPATH,using="//*[@id=\"selectedChild\"]")
    static WebElement childDropDown;
    @FindBy(how=How.XPATH,using="//*[@id=\"selectedInfants\"]")
    static WebElement infantDropDown;
    @FindBy(how=How.XPATH,using="//*[@class=\"totalCost\"]")
    static WebElement totalOverAllPrice;
    @FindBy(how=How.XPATH,using = "(//th[@scope=\"row\"])[1]")
    static WebElement adultPrice;
    @FindBy(how=How.XPATH,using="(//th[@scope=\"row\"])[2]")
    static WebElement childPrice;
    @FindBy(how=How.XPATH,using="(//th[@scope=\"row\"])[3]")
    static WebElement infantPrice;
    @FindBy(how=How.XPATH,using="(//button[@type=\"submit\"])[2]")
    static WebElement submitBooking;
    @FindBy(how=How.XPATH,using="//*[contains(text(),\"Booking Summary\")]")
    static WebElement bookingSummary;
    @FindBy(how=How.XPATH,using="//*[@class=\"bgwhite table table-striped\"]")
    static WebElement getTheSearchList;

    public static void beforeTest() {
        WebDriverManager.getInstance(CHROME).setup();
        DesiredCapabilities acceptSSlCertificate = DesiredCapabilities.chrome();
        acceptSSlCertificate.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        driver = new ChromeDriver(acceptSSlCertificate);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        PageFactory.initElements(driver, CommonFns.class);
        driver.get("https://www.phptravels.net/");

    }

    public static void WaitforPageToLoad(String path) {
        WebDriverWait wait = new WebDriverWait(driver, 50);
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(path)));
    }

    public static void verifySearchHotel() {
        //acceptCookies.click();
        WaitforPageToLoad("//*[@id=\"s2id_location\"]");
        System.out.println(searchHotel.getText());
        clickCheckin.click();
        datePicker();
        WaitforPageToLoad( "((//a[@class=\"select2-choice select2-default\"])[4]//span)[1]" );
        checkIn.click();
        checkIn.sendKeys("17/10/2019");
        checkOut.sendKeys( "18/10/2019" );
        members.clear();
        members.sendKeys( "1" );
        searchSubmit.click();

    }
    public static void searchList(){
        WebElement tests=driver.findElement(By.xpath("//*[@class=\"select2-input\"]"));
        tests.sendKeys("Turkey Creek, Australia");
        driver.findElement(By.xpath("(//button[@type=\"submit\"])[1]")).click();
        WaitforPageToLoad("//*[@class=\"bgwhite table table-striped\"]");
        getTheSearchList.isDisplayed();
    }

    public static void bookFeatureTour(){
        navigateBackToHomePage.isDisplayed();
        navigateBackToHomePage.click();
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,1000)");
       // List<WebElement> featuredTourCount=driver.findElements( By.xpath( "(//*[@class=\"container\"])[5]//following::h4/span" ) );
        for(int i=0;i<=featuredTourCount.size()-1;i++){
            int j=i+1;
            String getCountry=featuredTourCount.get(j).getText();
            System.out.println( getCountry );
            js.executeScript("window.scrollBy(500,0)");
            if(featuredTourCount.get( i ).getText().equals("Spectaculars Of The Nile…")){
                System.out.println( "hello world" );
                driver.findElement( By.xpath("(//*[@class=\"button btn-small\"])["+j+"]" ) ).click();
            }
            break;
        }

    }

    public static void datePicker() {
        // WebElement dateWidget = driver.findElement(your locator);
        List<WebElement> columns = getDate.findElements(By.tagName("td"));
            System.out.println(columns);
        for (WebElement cell : columns) {
            //Select 13th Date
            if (cell.getText().equals("13")) {
                cell.findElement(By.linkText("13"));
                cell.findElement(By.linkText("13")).click();
                break;
            }
        }


    }
    public static void navigationWondows(){
        navigateBackToHomePage.isDisplayed();
        navigateBackToHomePage.click();
    }
    public static void scrollDown(int up,int down){
        JavascriptExecutor js = (JavascriptExecutor) driver;
        String test=up+","+down;
        Object o = js.executeScript("window.scrollBy(" + test + ")");

    }

    public static void featuredBookingConfirmation(){
        scrollDown(0,1000);
        changeFetaureBookingDate.clear();
        changeFetaureBookingDate.sendKeys("30/9/2019");
        selectFromDropDown(adultDropDownList,"2");
        selectFromDropDown(childDropDown,"1");
        selectFromDropDown(infantDropDown,"1");
        //priceValidation(2,1,1);
        submitBooking.click();
    }
    public static void bookingConfirmationSummary(){
        bookingSummary.isDisplayed();
    }
    public static void quiteDriver(){
        driver.quit();
    }

    public static void selectFromDropDown(WebElement element, String value){

        Select adultList=new Select(element);
        adultList.selectByValue(value);

    }
    public static int defaultPrice()throws NumberFormatException{
        WaitforPageToLoad("//*[@class=\"totalCost\"]");
        String totalPrice=totalOverAllPrice.getText();
        String spiltPrice=totalPrice.substring(5,8);
        int totalBookingPrice=Integer.parseInt(spiltPrice);
        System.out.println(totalBookingPrice);
        return totalBookingPrice;
    }
    public static void priceValidation(int noOfAdult,int noOfChild,int noOfInfant){

        CommonFns.defaultPrice();
        String getAdultPrice=adultPrice.getText();
        String spiltAdultPrice=getAdultPrice.substring(8,11);
        int getDefaultAdultPrice=Integer.parseInt(spiltAdultPrice);
       // System.out.println(getDefaultAdultPrice);

        String getChildPrice=childPrice.getText();
        String spiltChildPrice=getChildPrice.substring(7,9);
        int getDefaultChildPrice=Integer.parseInt(spiltChildPrice);
       // System.out.println(getDefaultChildPrice);

        String getInfantPrice=infantPrice.getText();
        String spiltInfantPrice=getInfantPrice.substring(8,10);
        int getDefaultInfantPrice= Integer.parseInt(spiltInfantPrice);
       // System.out.println(getDefaultInfantPrice);


        int calculateTotalPrice=getDefaultAdultPrice*noOfAdult+getDefaultChildPrice*noOfChild+getDefaultInfantPrice*noOfInfant;
        int test=CommonFns.defaultPrice();
        System.out.print(calculateTotalPrice+CommonFns.defaultPrice());
        //Assert.assertEquals(calculateTotalPrice,test);
    }


}
