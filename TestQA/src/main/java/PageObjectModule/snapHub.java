package PageObjectModule;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static io.github.bonigarcia.wdm.DriverManagerType.CHROME;

public class snapHub {
    WebDriver driver;

    @FindBy(how = How.XPATH, using = "//*[@class=\"signin input\"]//input[@id=\"email\"]")
    static WebElement emailID;
    @FindBy(how=How.CSS,using="#password")
    static WebElement password;
    @FindBy(how=How.XPATH,using="//input[@name=\"Submit\"]")
    static WebElement loginSubmit;
    @FindBy(how=How.XPATH, using="//*[@class=\"user-greet intro-item\"]")
    static WebElement greetings;
    @FindBy(how=How.XPATH,using="//*[@id=\"accordion-item-live\"]")
    static WebElement live;
    @FindBy(how=How.XPATH,using="//*[@class=\"avatar\"]")
    static WebElement selectToLogout;
    @FindBy(how=How.XPATH,using="(//ul[@class=\"menu Dropdowncss__StyledMenu-sc-1evy82m-1 fwShwo\"]//li)[3]")
    static WebElement logout;
    @FindBy(how=How.XPATH,using="//button[@name=\"ok\"]")
    static WebElement confirmLogOut;

    public snapHub(WebDriver driver) {
        this.driver=driver;
        PageFactory.initElements(driver,this);
    }

    public void beforeStart()
    {
        WebDriverManager.getInstance(CHROME).setup();
        DesiredCapabilities acceptSSlCertificate = DesiredCapabilities.chrome();
        acceptSSlCertificate.setCapability( CapabilityType.ACCEPT_SSL_CERTS, true );
        driver = new ChromeDriver( acceptSSlCertificate );
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait( 10, TimeUnit.SECONDS );
        PageFactory.initElements(driver, snapHub.class);
    }
    public void loginPage(String emailId,String pwd,String url){
        beforeStart();
        driver.get(url);
        emailID.sendKeys(emailId);
        password.sendKeys(pwd);
        loginSubmit.click();
    }

    public void verifyHomePage() throws IOException {
        try {
            String showLive = live.getText();
            assert (showLive).equals( "LIVE" );
            takeScreenShot();
        }
        catch (NoSuchWindowException e){
                System.out.println( "Application not launched " );
        }

    }
    public void takeScreenShot() throws IOException {
        File screenshotFile = ((TakesScreenshot)driver).getScreenshotAs( OutputType.FILE);
        Date date=new Date();
        long currentTime=date.getTime();
        FileUtils.copyFile(screenshotFile, new File("target/screenShots/screenshot"+currentTime+".png"));
    }
    public void logout(){

        selectToLogout.click();
        logout.click();
        confirmLogOut.click();
        driver.quit();
    }
    public void waitForElement(WebElement path){
        WebDriverWait wait=new WebDriverWait( driver,20 );
        wait.until( ExpectedConditions.visibilityOf( (WebElement) By.xpath( String.valueOf( path ) ) ) );
    }
}