package PageObjectModule;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import static io.github.bonigarcia.wdm.DriverManagerType.CHROME;

public class SnapEngageHomePage {
    WebDriver driver;
    WebDriverWait wait;
    @FindBy(how=How.XPATH, using="//*[@class=\"user-greet intro-item\"]")
    private WebElement greetings;
    @FindBy(how=How.XPATH,using="//*[@id=\"accordion-item-live\"]")
    private WebElement live;
    @FindBy(how=How.XPATH,using="//button[@data-testid=\"dismiss-sound-pop-up\"]")
    private WebElement closePopup;

    public SnapEngageHomePage(WebDriver driver) {
        this.driver=driver;

        PageFactory.initElements(driver,this);
    }

    public void beforeStart() throws InterruptedException {
        wait.until( ExpectedConditions.presenceOfElementLocated( (By) greetings ));
        greetings.getText();
        closePopup.click();
        live.isDisplayed();
    }
    public void WaitforPageToLoad(){
       // wait = new WebDriverWait(driver, 10);

    }


}