package managers;

import org.openqa.selenium.WebDriver;
import PageObjectModule.snapHub;
import PageObjectModule.SnapEngageHomePage;
public class PageObjectManager {


    private WebDriver driver;
    private snapHub snapHubVerify;
    private SnapEngageHomePage homePage;

    public PageObjectManager(WebDriver driver) {

        this.driver = driver;

    }
    public snapHub login(){

        return (snapHubVerify == null) ? snapHubVerify = new snapHub( driver ) : snapHubVerify;

    }


}
