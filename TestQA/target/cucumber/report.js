$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/main/java/setFeature.feature");
formatter.feature({
  "name": "Automate to test",
  "description": "  Description:The purpose is to test SnapEngage functionality",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "login and Assert the HUb is loaded and Take a screenshot",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "login to the application",
  "keyword": "Given "
});
formatter.match({
  "location": "SnapEngageDef.java:29"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Assert that the hub is loaded",
  "keyword": "When "
});
formatter.match({
  "location": "SnapEngageDef.java:35"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "close the browser",
  "keyword": "Then "
});
formatter.match({
  "location": "SnapEngageDef.java:40"
});
formatter.result({
  "status": "passed"
});
});